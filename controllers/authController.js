const crypto = require('crypto');
const nodemailer = require('nodemailer');
const { promisify } = require('util');
const jwt = require('jsonwebtoken');
const handlebars = require('handlebars');
const fs = require('fs');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');

const User = require('./../models/userModel');
const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/appError');
const sendEmail = require('./../utils/email');

const readHTMLFile = function (path, callback) {
	fs.readFile(path, { encoding: 'utf-8' }, function (err, html) {
		if (err) {
			throw err;
			callback(err);
		} else {
			callback(null, html);
		}
	});
};

const signToken = (id) => {
	return jwt.sign({ id }, process.env.JWT_SECRET, {
		expiresIn: '2d',
	});
};

const createSendToken = (user, statusCode, res) => {
	const token = signToken(user._id);
	const cookieOptions = {
		expires: new Date(
			Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000
		),
		httpOnly: true,
	};
	if (process.env.NODE_ENV === 'production') cookieOptions.secure = true;

	res.cookie('jwt', token, cookieOptions);

	// Remove password from output
	user.password = undefined;

	res.status(statusCode).json({
		status: 'success',
		token,
		data: {
			user,
		},
	});
};

exports.signup = catchAsync(async (req, res, next) => {
	const newUser = await User.create({
		name: req.body.name,
		email: req.body.email,
		password: req.body.password,
		passwordConfirm: req.body.passwordConfirm,
	});

	// 2FA

	createSendToken(newUser, 201, res);
});

// Login OTP
// exports.login = catchAsync(async (req, res, next) => {
// 	const { email, password } = req.body;
// 	console.log(email)
// 	console.log(password);

// 	// 1) Check if email and password exist
// 	if (!email || !password) {
// 		return next(new AppError('Please provide email and password!', 400));
// 	}
// 	// 2) Check if user exists && password is correct
// 	const user = await User.findOne({ email }).select('+password');

// 	if (!user || !(await user.correctPassword(password, user.password))) {
// 		return next(new AppError('Incorrect email or password', 401));
// 	}

// 	console.log(req.headers['x-otp']);
// 	//check if otp is passed, if not then ask for OTP
// 	if (!req.headers['x-otp']) {
// 		return next(new AppError('No OTP found', 400));
// 	}

// 	//validate otp
// 	const verified = speakeasy.totp.verify({
// 		secret: user.twofactor.secret,
// 		encoding: 'base32',
// 		token: req.headers['x-otp'],
// 	});

// 	if (verified) {
// 		// 3) If everything ok, send token to client
// 		createSendToken(user, 200, res);
// 		return res.send('success');
// 	} else {
// 		return next(new AppError('Invalid OTP', 400));
// 	}
// });

exports.login = catchAsync(async (req, res, next) => {
	const { email, password } = req.body;

	// 1) Check if email and password exist
	if (!email || !password) {
		return next(new AppError('Please provide email and password!', 400));
	}
	// 2) Check if user exists && password is correct
	const user = await User.findOne({ email }).select(
		'+password +isTwoFactor',
	);

	if (!user || !(await user.correctPassword(password, user.password))) {
		return next(new AppError('Incorrect email or password', 401));
	}
	console.log(user.isTwoFactor)
	// If user using two factor
	if (user.isTwoFactor) {
		if (!req.headers['x-otp']) {
			return next(new AppError('No OTP found', 400));
		}

		//validate otp
		const verified = speakeasy.totp.verify({
			secret: user.twofactor.secret,
			encoding: 'base32',
			token: req.headers['x-otp'],
		});

		if (verified) {
			// 3) If everything ok, send token to client
			createSendToken(user, 200, res);
			return res.send('success');
		} else {
			return next(new AppError('Invalid OTP', 400));
		}
	} else {
		// 3) If everything ok, send token to client
		createSendToken(user, 200, res);
	}

});

exports.protect = catchAsync(async (req, res, next) => {
	// 1) Getting token and check of it's there
	let token;
	if (
		req.headers.authorization &&
		req.headers.authorization.startsWith('Bearer')
	) {
		token = req.headers.authorization.split(' ')[1];
	}

	if (!token) {
		return next(
			new AppError('You are not logged in! Please log in to get access.', 401)
		);
	}

	// 2) Verification token
	const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

	// 3) Check if user still exists
	const currentUser = await User.findById(decoded.id);
	if (!currentUser) {
		return next(
			new AppError(
				'The user belonging to this token does no longer exist.',
				401
			)
		);
	}

	// 4) Check if user changed password after the token was issued
	if (currentUser.changedPasswordAfter(decoded.iat)) {
		return next(
			new AppError('User recently changed password! Please log in again.', 401)
		);
	}

	// GRANT ACCESS TO PROTECTED ROUTE
	req.user = currentUser;
	next();
});

exports.restrictTo = (...roles) => {
	return (req, res, next) => {
		// roles ['admin', 'lead-guide']. role='user'
		if (!roles.includes(req.user.role)) {
			return next(
				new AppError('You do not have permission to perform this action', 403)
			);
		}

		next();
	};
};

exports.forgotPassword = catchAsync(async (req, res, next) => {
	// 1) Get user based on POSTed email
	const user = await User.findOne({ email: req.body.email });
	if (!user) {
		return next(new AppError('There is no user with email address.', 404));
	}

	// 2) Generate the random reset token
	const resetToken = user.createPasswordResetToken();
	await user.save({ validateBeforeSave: false });
	const url = req.body.url;

	// 3) Send it to user's email
	// const resetURL = `${req.protocol}://${req.get(
	// 	'host'
	// )}/api/v1/users/resetPassword/${resetToken}`
	// const resetURL = `${url}/${resetToken}`
	// const message = `Quên mật khẩu? Hãy bấm vào đường link sau để tạo mật khẩu mới: ${resetURL}.`

	let transporter = nodemailer.createTransport({
		host: 'smtp.vccloud.vn',
		port: 587,
		secure: false,
		auth: {
			user: process.env.EMAIL,
			pass: process.env.PASSWORD,
		},
	});

	readHTMLFile(
		process.cwd() + '/public/templates/forgot.html',
		function (err, html) {
			const template = handlebars.compile(html);
			const replacements = {
				url: url,
				token: resetToken,
			};
			const htmlToSend = template(replacements);
			let mailOptions = {
				from: process.env.EMAIL,
				to: `${user.email}`,
				subject: 'Reset Password',
				html: htmlToSend,
				// text: message,
			};

			transporter.sendMail(mailOptions, (err, data) => {
				if (err) {
					console.log(err);
					return next(new AppError('Cant send email', 500));
				}

				res.status(200).json({
					status: 'success',
					message: 'Token sent to email!',
				});
			});
		}
	);
});

exports.resetPassword = catchAsync(async (req, res, next) => {
	// 1) Get user based on the token
	const hashedToken = crypto
		.createHash('sha256')
		.update(req.params.token)
		.digest('hex');

	const user = await User.findOne({
		passwordResetToken: hashedToken,
		passwordResetExpires: { $gt: Date.now() },
	});

	// 2) If token has not expired, and there is user, set the new password
	if (!user) {
		return next(new AppError('Token is invalid or has expired', 400));
	}
	user.password = req.body.password;
	user.passwordConfirm = req.body.passwordConfirm;
	user.passwordResetToken = undefined;
	user.passwordResetExpires = undefined;
	await user.save();

	// 3) Update changedPasswordAt property for the user
	// 4) Log the user in, send JWT
	createSendToken(user, 200, res);
});

exports.updatePassword = catchAsync(async (req, res, next) => {
	// 1) Get user from collection
	const user = await User.findById(req.user.id).select('+password');
	// console.log(req.body.passwordCurrent)

	// 2) Check if POSTed current password is correct
	if (!(await user.correctPassword(req.body.passwordCurrent, user.password))) {
		return next(new AppError('Your current password is wrong.', 401));
	}

	// 3) If so, update password
	user.password = req.body.password;
	user.passwordConfirm = req.body.passwordConfirm;
	await user.save();
	// User.findByIdAndUpdate will NOT work as intended!

	// 4) Log user in, send JWT
	createSendToken(user, 200, res);
});

exports.setupTwoFactor = catchAsync(async (req, res, next) => {
	// const user = await User.findOne({ email: req.body.email });
	const user = req.user;
	console.log(user);
	if (!user) {
		return next(new AppError('There is no user with email address.', 404));
	}

	const secret = speakeasy.generateSecret({ length: 10 });
	QRCode.toDataURL(secret.otpauth_url, async (err, data_url) => {
		//save to logged in user.
		user.twofactor = {
			secret: '',
			tempSecret: secret.base32,
			dataURL: data_url,
			otpURL: secret.otpauth_url,
		};
		await user.save({ validateBeforeSave: false });

		return res.json({
			message: 'Verify OTP',
			tempSecret: secret.base32,
			dataURL: data_url,
			otpURL: secret.otpauth_url,
		});
	});
});

exports.verifyTwoFactor = catchAsync(async (req, res, next) => {
	// const user = await User.findOne({ email: req.body.email });
	const user = req.user;
	console.log(user);
	if (!user) {
		return next(new AppError('There is no user with email address.', 404));
	}

	console.log(user.twofactor.tempSecret);
	console.log(req.body.token);

	const verified = speakeasy.totp.verify({
		secret: user.twofactor.tempSecret, //secret of the logged in user
		encoding: 'base32',
		token: req.body.token,
	});
	if (verified) {
		user.twofactor.secret = user.twofactor.tempSecret;
		await user.save({ validateBeforeSave: false });
		return res.send('Two-factor auth enabled');
	}
	return next(new AppError('Invalid token, verification failed', 400));
});
