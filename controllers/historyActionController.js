const catchAsync = require('../utils/catchAsync');
const APIFeatures = require('./../utils/apiFeatures');
const HistoryAction = require('./../models/historyActionModel');
const User = require('./../models/userModel');
const AppError = require('./../utils/appError');

exports.getAllHistoryAction = catchAsync(async (req, res, next) => {
	const features = new APIFeatures(HistoryAction.find(), req.query).filter();
	const docLength = await features.query;
	
	features.sort().limitFields().paginate();

	const historyActions = await features.query;
	const dataPromise = historyActions.map(async (historyAction) => {
		const user = await User.findById(historyAction.user);
		return { ...historyAction._doc, email: user.email };
	});

	let data = await Promise.allSettled(dataPromise);
	data = data.map(el => el.value)

	// SEND RESPONSE
	res.status(200).json({
		status: 'success',
		docLength: docLength.length,
		actions: data,
	});
});

exports.getHistoryAction = catchAsync(async (req, res, next) => {
	const historyAction = await HistoryAction.findById(req.params.id);

	if (!historyAction) {
		return next(new AppError('No action found with that ID', 404));
	}

	const data = { ...historyReport._doc, email: req.user.email };

	res.status(200).json({
		status: 'success',
		action: data,
	});
});

exports.createHistoryAction = catchAsync(async (req, res, next) => {
	const reqHistoryAction = { ...req.body, user: req.user };
	const newHistoryAction = await HistoryAction.create(reqHistoryAction);

	res.status(201).json({
		status: 'success',
		action: newHistoryAction,
	});
});
