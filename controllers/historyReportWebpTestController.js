const catchAsync = require('../utils/catchAsync');
const APIFeatures = require('./../utils/apiFeatures');
const HistoryReportWebpTest = require('./../models/historyReportWebpTestModel');
const User = require('./../models/userModel');
const AppError = require('./../utils/appError');

exports.getAllHistoryReportWebpTest = catchAsync(async (req, res, next) => {
	const features = new APIFeatures(
		HistoryReportWebpTest.find({ user: req.user._id }),
		req.query
	).filter();
	const docLength = await features.query;
	features.sort().limitFields().paginate();

	const historyReports = await features.query;
	const data = historyReports.map(historyReport => {
		return { ...historyReport._doc, email: req.user.email };
	});

	// SEND RESPONSE
	res.status(200).json({
		status: 'success',
		docLength: docLength.length,
		reports: data,
	});
});

exports.getHistoryReportWebpTest = catchAsync(async (req, res, next) => {
	const historyReport = await HistoryReportWebpTest.findById(req.params.id);

	if (!historyReport) {
		return next(new AppError('No report found with that ID', 404));
	}

	const data = { ...historyReport._doc, email: req.user.email };

	res.status(200).json({
		status: 'success',
		report: data,
	});
});

exports.createHistoryReportWebpTest = catchAsync(async (req, res, next) => {
	const IdTestExist = await HistoryReportWebpTest.findOne({
		idTest: req.body.idTest,
	});

	if (IdTestExist) {
		return next(new AppError('ID Test exists', 400));
	}

	const data = { ...req.body, user: req.user._id };
	const newHistoryReport = await HistoryReportWebpTest.create(data);

	res.status(201).json({
		status: 'success',
		report: newHistoryReport,
	});
});
