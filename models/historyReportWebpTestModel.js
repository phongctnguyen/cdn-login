const mongoose = require('mongoose');

const historyReportWebpTestSchema = new mongoose.Schema({
	idTest: {
		type: String,
		required: [true, 'Please provide id test'],
	},
	domainOrigin: {
		type: String,
		required: [true, 'Please provide domain origin'],
	},
	domainEmulator: {
		type: String,
		required: [true, 'Please provide domain emulator'],
	},
	location: {
		type: String,
		required: [true, 'Please provide location'],
	},
	hostIP: {
		type: String,
	},
	isp: {
		type: String,
		required: [true, 'Please provide isp'],
	},
	createdAt: {
		type: Date,
		default: Date.now,
	},
	user: {
		type: mongoose.Schema.ObjectId,
		ref: 'User',
		required: [true, 'user is required'],
	},
});

const HistoryReportWebpTest = mongoose.model(
	'historyReportWebpTest',
	historyReportWebpTestSchema
);

module.exports = HistoryReportWebpTest;
