const mongoose = require('mongoose');
const validator = require('validator');
const User = require('./userModel');

const historyActionSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Person',
		required: [true],
	},
	action: {
		type: String,
	},
	message: {
		type: String,
	},
	createdAt: {
		type: Date,
		default: Date.now,
	},
	user: {
		type: mongoose.Schema.ObjectId,
		ref: 'User',
		required: [true, 'user is required'],
	},
});

const HistoryAction = mongoose.model('HistoryAction', historyActionSchema);

module.exports = HistoryAction;
