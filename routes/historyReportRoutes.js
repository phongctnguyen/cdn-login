const express = require('express');
const authController = require('./../controllers/authController');
const historyReportController = require('./../controllers/historyReportController');

const router = express.Router();

router
	.get(
		'/',
		authController.protect,
		// authController.restrictTo('admin'),
		historyReportController.getAllHistoryReport
	)
	.post(
		'/',
		authController.protect,
		historyReportController.createHistoryReport
	);

router
	.route('/:id')
	.get(authController.protect, historyReportController.getHistoryReport);

module.exports = router;
