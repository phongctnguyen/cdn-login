const express = require('express');
const authController = require('./../controllers/authController');
const historyReportWebpTestController = require('./../controllers/historyReportWebpTestController');

const router = express.Router();

router
	.get(
		'/',
		authController.protect,
		// authController.restrictTo('admin'),
		historyReportWebpTestController.getAllHistoryReportWebpTest
	)
	.post(
		'/',
		authController.protect,
		historyReportWebpTestController.createHistoryReportWebpTest
	);

router
	.route('/:id')
	.get(
		authController.protect,
		historyReportWebpTestController.getHistoryReportWebpTest
	);

module.exports = router;
