const express = require('express');
const authController = require('./../controllers/authController');
const historyActionController = require('./../controllers/historyActionController');

const router = express.Router();

router
	.get(
		'/',
		authController.protect,
		authController.restrictTo('admin'),
		historyActionController.getAllHistoryAction
	)
	.post(
		'/',
		authController.protect,
		historyActionController.createHistoryAction
	);

router
	.route('/:id')
	.get(authController.protect, historyActionController.getHistoryAction);

module.exports = router;
