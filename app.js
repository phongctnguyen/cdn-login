const express = require('express')

const app = express();

const userRouter = require('./routes/userRoutes')
const historyActionRouter = require('./routes/historyActionRoutes');
const historyReportRouter = require('./routes/historyReportRoutes');
const historyReportWebpTestRouter = require('./routes/historyReportWebpTestRoutes');
const authRouter = require('./routes/authRoutes')
const AppError = require('./utils/appError')
const globalErrorHandler = require('./controllers/errorController')

// Serving static files
app.use(express.static(`${__dirname}/public`));
app.use(express.json())
app.use('/api/v1/users', userRouter);
app.use('/api/v1/history/action', historyActionRouter);
app.use('/api/v1/history/report', historyReportRouter);
app.use('/api/v1/history/report/webp', historyReportWebpTestRouter);
// app.use('/auth/api/v1/users', authRouter);

app.all('*', (req, res, next) => {
	next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404))
})

app.use(globalErrorHandler)

module.exports = app;